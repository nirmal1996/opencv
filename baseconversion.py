
# import base64
 
# with open("opencv0.jpg", "rb") as imageFile:
#     str = base64.b64encode(compress(imageFile.read()))
#     print (str)

import base64
from io import BytesIO
from PIL import Image


image_data = bytes(image_data, encoding="ascii")
im = Image.open(BytesIO(base64.b64decode(image_data)))
im.save('opencv.jpg')