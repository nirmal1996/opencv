from camera import VideoCamera
from flask_restful import Api, Resource
import os
from flask import Flask
api = Api()
app=Flask(__name__)

api.add_resource(VideoCamera, '/VideoCamera', endpoint="VideoCamera")
if __name__=="__main__":
    app.run()